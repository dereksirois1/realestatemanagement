﻿/*
Post-Deployment Script Template							
--------------------------------------------------------------------------------------
 This file contains SQL statements that will be appended to the build script.		
 Use SQLCMD syntax to include a file in the post-deployment script.			
 Example:      :r .\myfile.sql								
 Use SQLCMD syntax to reference a variable in the post-deployment script.		
 Example:      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/
IF (NOT EXISTS(SELECT * FROM [dbo].[UnitType] WHERE Name = 'Residential'))
BEGIN
    INSERT INTO [dbo].[UnitType](Name, Description)
    VALUES('Residential', 'Residential unit')
END

IF (NOT EXISTS(SELECT * FROM [dbo].[UnitType] WHERE Name = 'Commercial'))
BEGIN
    INSERT INTO [dbo].[UnitType](Name, Description)
    VALUES('Commercial', 'Commercial unit')
END

IF (NOT EXISTS(SELECT * FROM [dbo].[MaintenanceStatus] WHERE Name = 'New'))
BEGIN
    INSERT INTO [dbo].[MaintenanceStatus](Name)
    VALUES('New')
END

IF (NOT EXISTS(SELECT * FROM [dbo].[MaintenanceStatus] WHERE Name = 'Approved'))
BEGIN
    INSERT INTO [dbo].[MaintenanceStatus](Name)
    VALUES('Approved')
END

IF (NOT EXISTS(SELECT * FROM [dbo].[MaintenanceStatus] WHERE Name = 'In Progress'))
BEGIN
    INSERT INTO [dbo].[MaintenanceStatus](Name)
    VALUES('In Progress')
END

IF (NOT EXISTS(SELECT * FROM [dbo].[MaintenanceStatus] WHERE Name = 'Completed'))
BEGIN
    INSERT INTO [dbo].[MaintenanceStatus](Name)
    VALUES('Completed')
END

IF (NOT EXISTS(SELECT * FROM [dbo].[MaintenanceStatus] WHERE Name = 'Rejected'))
BEGIN
    INSERT INTO [dbo].[MaintenanceStatus](Name)
    VALUES('Rejected')
END
