﻿namespace RealEstateManagement.API.Models
{
    public class JsonResponse
    {
        public string Message { get; set; }
        public string Token { get; set; }
    }
}
