﻿namespace RealEstateManagement.API.Models.Dtos
{
    public class PropertyDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public Guid UserId { get; set; }
    }
}
