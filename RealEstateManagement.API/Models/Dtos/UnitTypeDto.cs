﻿namespace RealEstateManagement.API.Models.Dtos
{
    public class UnitTypeDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
