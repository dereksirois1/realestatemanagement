﻿using RealEstateManagement.API.Models.Entities;

namespace RealEstateManagement.API.Models.Dtos
{
    public class UnitDto
    {
        public Guid Id { get; set; }
        public string DoorNumber { get; set; }
        public double Rent { get; set; }
        public Guid PropertyId { get; set; }
        public Guid UnitTypeId { get; set; }
    }
}
