﻿namespace RealEstateManagement.API.Models.Dtos
{
    public class MaintenanceStatusDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}
