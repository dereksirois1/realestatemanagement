﻿using RealEstateManagement.API.Models.Entities;

namespace RealEstateManagement.API.Models.Dtos
{
    public class MaintenanceDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public Guid StatusId { get; set; }
        public Guid UnitId { get; set; }
    }
}
