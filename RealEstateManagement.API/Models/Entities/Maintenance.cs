﻿namespace RealEstateManagement.API.Models.Entities
{
    public class Maintenance
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public Guid StatusId { get; set; }
        public MaintenanceStatus Status { get; set; }
        public Guid UnitId { get; set; }
        public Unit Unit { get; set; }
    }
}
