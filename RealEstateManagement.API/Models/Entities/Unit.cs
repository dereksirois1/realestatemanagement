﻿namespace RealEstateManagement.API.Models.Entities
{
    public class Unit
    {
        public Guid Id { get; set; }
        public string DoorNumber { get; set; }
        public double Rent { get; set; }
        public Guid PropertyId { get; set; }
        public Property Property { get; set; } = null!;
        public Guid UnitTypeId { get; set; }
        public UnitType UnitType { get; set; } = null!;
        public ICollection<Maintenance> Maintenance { get; set; }
    }
}
