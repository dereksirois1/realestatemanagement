﻿namespace RealEstateManagement.API.Models.Entities
{
    public class MaintenanceStatus
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public ICollection<Maintenance> Maintenance { get; set; }
    }
}
