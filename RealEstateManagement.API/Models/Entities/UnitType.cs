﻿namespace RealEstateManagement.API.Models.Entities
{
    public class UnitType
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public ICollection<Unit> Unit { get; set; } = new List<Unit>();
    }
}
