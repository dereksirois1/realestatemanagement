﻿namespace RealEstateManagement.API.Models.Entities
{
    public class Property
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public Guid UserId { get; set; }
        public User User { get; set; } = null!;
        public ICollection<Unit> Unit { get; set; } = new List<Unit>();
    }
}
