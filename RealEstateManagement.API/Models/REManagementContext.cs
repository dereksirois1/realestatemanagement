﻿using Microsoft.EntityFrameworkCore;
using RealEstateManagement.API.Models.Entities;
using System.Reflection.Metadata;

namespace RealEstateManagement.API.Models
{
    public class REManagementContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Property> Properties { get; set; }
        public DbSet<UnitType> UnitType { get; set; }
        public DbSet<Unit> Units { get; set; }
        public DbSet<MaintenanceStatus> MaintenanceStatus { get; set; }
        public DbSet<Maintenance> Maintenance { get; set; }

        public REManagementContext(DbContextOptions options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>()
                .HasMany(e => e.Properties)
                .WithOne(e => e.User)
                .HasForeignKey(e => e.UserId)
                .IsRequired();

            modelBuilder.Entity<UnitType>()
                .HasMany(e => e.Unit)
                .WithOne(e => e.UnitType)
                .HasForeignKey(e => e.UnitTypeId)
                .IsRequired();

            modelBuilder.Entity<Property>()
                .HasMany(e => e.Unit)
                .WithOne(e => e.Property)
                .HasForeignKey(e => e.PropertyId)
                .IsRequired();

            modelBuilder.Entity<MaintenanceStatus>()
                .HasMany(e => e.Maintenance)
                .WithOne(e => e.Status)
                .HasForeignKey(e => e.StatusId)
                .IsRequired();

            modelBuilder.Entity<Unit>()
                .HasMany(e => e.Maintenance)
                .WithOne(e => e.Unit)
                .HasForeignKey(e => e.UnitId)
                .IsRequired();
        }
    }
}
