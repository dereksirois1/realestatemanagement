﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RealEstateManagement.API.Services.Interfaces;

namespace RealEstateManagement.API.Controllers
{
    [Route("api/[controller]")]
    [Authorize]
    [ApiController]
    public class MaintenanceStatusController : ControllerBase
    {
        private readonly IMaintenanceStatusService _service;

        public MaintenanceStatusController(IMaintenanceStatusService service)
        {
            _service = service;
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            try
            {
                var entities = _service.GetAll();
                if (entities == null || entities.Count() == 0)
                {
                    return NoContent();
                }
                return Ok(entities);
            }
            catch (Exception)
            {
                return StatusCode(500);
            }
        }

        [HttpGet("{id:guid}")]
        public IActionResult GetById(Guid id)
        {
            try
            {
                var entity = _service.GetById(id);
                if (entity == null)
                {
                    return NoContent();
                }
                return Ok(entity);
            }
            catch (Exception)
            {
                return StatusCode(500);
            }
        }
    }
}
