﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RealEstateManagement.API.Helper;
using RealEstateManagement.API.Models.Dtos;
using RealEstateManagement.API.Services.Interfaces;

namespace RealEstateManagement.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MaintenanceController : ControllerBase
    {
        private readonly IMaintenanceService _service;
        private readonly IAuthHelper _authHelper;

        public MaintenanceController(IMaintenanceService unitService, IAuthHelper authHelper)
        {
            _service = unitService;
            _authHelper = authHelper;
        }

        [HttpGet("{unitId:guid}")]
        public IActionResult GetAll(Guid unitId)
        {
            try
            {
                var accessToken = _authHelper.GetAccessToken(Request);
                var userId = _authHelper.GetAuthenticatedUserId(accessToken!);
                var properties = _service.GetAllByUnit(userId, unitId);

                if (properties == null)
                {
                    return NotFound();
                }

                return Ok(properties);
            }
            catch (Exception)
            {
                return StatusCode(500);
            }
        }

        [HttpGet("{unitId:guid}/{id:guid}")]
        public IActionResult GetById(Guid unitId, Guid id)
        {
            try
            {
                var unit = _service.GetAllByUnit(id, unitId);

                if (unit == null)
                {
                    return NotFound();
                }

                return Ok(unit);
            }
            catch (Exception)
            {
                return StatusCode(500);
            }
        }

        [HttpPost("{unitId:guid}")]
        public IActionResult Create(Guid unitId, [FromBody] MaintenanceDto maintenance)
        {
            try
            {
                maintenance.UnitId = unitId;
                _service.Create(maintenance);
                return Ok();
            }
            catch (Exception)
            {
                return StatusCode(500);
            }
        }

        [HttpPut("{unitId:guid}/{id:guid}")]
        public IActionResult Create(Guid unitId, Guid id, [FromBody] MaintenanceDto maintenance)
        {
            try
            {
                maintenance.Id = id;
                maintenance.UnitId = unitId;
                _service.Update(maintenance);
                return Ok();
            }
            catch (Exception)
            {
                return StatusCode(500);
            }
        }

        [HttpDelete("{unitId:guid}/{id:guid}")]
        public IActionResult Delete(Guid unitId, Guid id)
        {
            try
            {
                var accessToken = _authHelper.GetAccessToken(Request);
                var userId = _authHelper.GetAuthenticatedUserId(accessToken!);
                _service.Delete(userId, id);
                return Ok();
            }
            catch (Exception)
            {
                return StatusCode(500);
            }
        }
    }
}
