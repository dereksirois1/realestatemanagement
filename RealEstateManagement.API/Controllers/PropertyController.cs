﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Net.Http.Headers;
using RealEstateManagement.API.Helper;
using RealEstateManagement.API.Models.Dtos;
using RealEstateManagement.API.Services.Interfaces;

namespace RealEstateManagement.API.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class PropertyController : ControllerBase
    {
        private readonly IPropertyService _propertyService;
        private readonly IAuthHelper _authHelper;

        public PropertyController(IPropertyService propertyService, IAuthHelper authHelper)
        {
            _propertyService = propertyService;
            _authHelper = authHelper;
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            try
            {
                var accessToken = _authHelper.GetAccessToken(Request);
                var userId = _authHelper.GetAuthenticatedUserId(accessToken!);
                var properties = _propertyService.GetAllByUser(userId);

                if (properties == null)
                {
                    return NotFound();
                }

                return Ok(properties);
            }
            catch (Exception)
            {
                return StatusCode(500);
            }
        }

        [HttpGet("{id:guid}")]
        public IActionResult GetById(Guid id)
        {
            try
            {
                var property = _propertyService.GetById(id);

                if (property == null)
                {
                    return NotFound();
                }

                return Ok(property);
            }
            catch (Exception)
            {
                return StatusCode(500);
            }
        }

        [HttpPost]
        public IActionResult Create([FromBody]PropertyDto property)
        {
            try
            {
                var accessToken = _authHelper.GetAccessToken(Request);
                property.UserId = _authHelper.GetAuthenticatedUserId(accessToken);
                _propertyService.Create(property);
                return Ok();
            }
            catch (Exception)
            {
                return StatusCode(500);
            }
        }

        [HttpPut("{id:guid}")]
        public IActionResult Create(Guid id, [FromBody] PropertyDto property)
        {
            try
            {
                property.Id = id;
                _propertyService.Update(property);
                return Ok();
            }
            catch (Exception)
            {
                return StatusCode(500);
            }
        }

        [HttpDelete("{id:guid}")]
        public IActionResult Delete(Guid id)
        {
            try
            {
                _propertyService.Delete(id);
                return Ok();
            }
            catch (Exception)
            {
                return StatusCode(500);
            }
        }
    }
}
