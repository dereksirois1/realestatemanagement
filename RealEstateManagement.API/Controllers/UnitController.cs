﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RealEstateManagement.API.Helper;
using RealEstateManagement.API.Models.Dtos;
using RealEstateManagement.API.Services.Interfaces;

namespace RealEstateManagement.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UnitController : ControllerBase
    {
        private readonly IUnitService _unitService;
        private readonly IAuthHelper _authHelper;

        public UnitController(IUnitService unitService, IAuthHelper authHelper)
        {
            _unitService = unitService;
            _authHelper = authHelper;
        }

        [HttpGet("{propertyId:guid}")]
        public IActionResult GetAll(Guid propertyId)
        {
            try
            {
                var accessToken = _authHelper.GetAccessToken(Request);
                var userId = _authHelper.GetAuthenticatedUserId(accessToken!);
                var properties = _unitService.GetAllByProperty(userId, propertyId);

                if (properties == null)
                {
                    return NotFound();
                }

                return Ok(properties);
            }
            catch (Exception)
            {
                return StatusCode(500);
            }
        }

        [HttpGet("{propertyId:guid}/{id:guid}")]
        public IActionResult GetById(Guid propertyId, Guid id)
        {
            try
            {
                var property = _unitService.GetAllByProperty(id, propertyId);

                if (property == null)
                {
                    return NotFound();
                }

                return Ok(property);
            }
            catch (Exception)
            {
                return StatusCode(500);
            }
        }

        [HttpPost("{propertyId:guid}")]
        public IActionResult Create(Guid propertyId, [FromBody] UnitDto unit)
        {
            try
            {
                unit.PropertyId = propertyId;
                _unitService.Create(unit);
                return Ok();
            }
            catch (Exception)
            {
                return StatusCode(500);
            }
        }

        [HttpPut("{propertyId:guid}/{id:guid}")]
        public IActionResult Create(Guid propertyId, Guid id, [FromBody] UnitDto unit)
        {
            try
            {
                unit.Id = id;
                unit.PropertyId = propertyId;
                _unitService.Update(unit);
                return Ok();
            }
            catch (Exception)
            {
                return StatusCode(500);
            }
        }

        [HttpDelete("{propertyId:guid}/{id:guid}")]
        public IActionResult Delete(Guid propertyId, Guid id)
        {
            try
            {
                var accessToken = _authHelper.GetAccessToken(Request);
                var userId = _authHelper.GetAuthenticatedUserId(accessToken!);
                _unitService.Delete(userId, id);
                return Ok();
            }
            catch (Exception)
            {
                return StatusCode(500);
            }
        }
    }
}
