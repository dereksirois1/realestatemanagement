﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RealEstateManagement.API.Helper;
using RealEstateManagement.API.Models;
using RealEstateManagement.API.Models.Dtos;
using RealEstateManagement.API.Services.Interfaces;
using System.Net;

namespace RealEstateManagement.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUserService _userService;
        public UserController(IUserService userService) 
        {
            _userService = userService;
        }

        [HttpPost("register")]
        public IActionResult Register([FromBody]UserDto user)
        {
            try
            {
                _userService.Create(user);
            }
            catch (Exception)
            {
                return StatusCode(500);
            }

            return Ok();
        }


        [HttpPost("login")]
        public IActionResult Login([FromBody]UserLoginDto user)
        {
            try
            {
                var token = _userService.Login(user);
                var res = new JsonResponse()
                {
                    Message = "You are logged in",
                    Token = token
                };
                return Ok(res);
            }
            catch (Exception e)
            {
                return StatusCode(500);
            }
        }
    }
}
