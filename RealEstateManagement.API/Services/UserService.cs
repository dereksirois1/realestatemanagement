﻿using AutoMapper;
using Microsoft.Extensions.Configuration.UserSecrets;
using RealEstateManagement.API.Config;
using RealEstateManagement.API.Helper;
using RealEstateManagement.API.Models.Dtos;
using RealEstateManagement.API.Models.Entities;
using RealEstateManagement.API.Repositories.Interfaces;
using RealEstateManagement.API.Services.Interfaces;

namespace RealEstateManagement.API.Services
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;
        private readonly IMapper _mapper;
        private readonly IAuthHelper _authHelper;
        private readonly JwtOptions _jwtOptions;

        public UserService(IUserRepository userRepository, IMapper mapper, IAuthHelper authHelper, JwtOptions jwtOptions)
        {
            _userRepository = userRepository;
            _mapper = mapper;
            _authHelper = authHelper;
            _jwtOptions = jwtOptions;
        }

        public UserDto GetById(Guid id)
        {
            var user = _userRepository.GetById(id);
            return _mapper.Map<UserDto>(user);
        }

        public UserDto GetByEmail(string email)
        {
            var user = _userRepository.GetByEmail(email);
            return _mapper.Map<UserDto>(user);
        }

        public string Login(UserLoginDto user)
        {
            var userDb = GetByEmail(user.Email);
            if (!BCrypt.Net.BCrypt.Verify(user.Password, userDb.Password))
            {
                throw new UnauthorizedAccessException();
            }

            var token = _authHelper.CreateAccessToken(_jwtOptions, user.Email, TimeSpan.FromMinutes(60), userDb.Id);
            return token;
        }

        public void Create(UserDto user)
        {
            var entity = _mapper.Map<User>(user);
            entity.Password = BCrypt.Net.BCrypt.HashPassword(entity.Password);
            _userRepository.Create(entity);
        }
    }
}
