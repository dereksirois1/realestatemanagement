﻿using AutoMapper;
using RealEstateManagement.API.Models.Dtos;
using RealEstateManagement.API.Repositories.Interfaces;
using RealEstateManagement.API.Services.Interfaces;

namespace RealEstateManagement.API.Services
{
    public class MaintenanceStatusService : IMaintenanceStatusService
    {
        private readonly IMaintenanceStatusRepository _repository;
        private readonly IMapper _mapper;

        public MaintenanceStatusService(IMaintenanceStatusRepository repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        public IEnumerable<MaintenanceStatusDto> GetAll()
        {
            var entities = _repository.GetAll();
            var entitiesDto = _mapper.Map<IEnumerable<MaintenanceStatusDto>>(entities);
            return entitiesDto;
        }

        public MaintenanceStatusDto GetById(Guid id)
        {
            var entity = _repository.GetById(id);
            var entityDto = _mapper.Map<MaintenanceStatusDto>(entity);
            return entityDto;
        }
    }
}
