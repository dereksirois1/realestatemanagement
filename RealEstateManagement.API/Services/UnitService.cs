﻿using AutoMapper;
using RealEstateManagement.API.Models.Dtos;
using RealEstateManagement.API.Models.Entities;
using RealEstateManagement.API.Repositories.Interfaces;
using RealEstateManagement.API.Services.Interfaces;

namespace RealEstateManagement.API.Services
{
    public class UnitService : IUnitService
    {
        private readonly IUnitRepository _repository;
        private readonly IMapper _mapper;

        public UnitService(IUnitRepository repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        public IEnumerable<UnitDto> GetAllByProperty(Guid userId, Guid propertyId)
        {
            var units = _repository.GetAllByProperty(userId, propertyId);
            var unitsDto = _mapper.Map<IEnumerable<UnitDto>>(units);
            return unitsDto;
        }

        public UnitDto GetById(Guid userId, Guid id)
        {
            var unit = _repository.GetById(userId, id);
            var unitDto = _mapper.Map<UnitDto>(unit);
            return unitDto;
        }

        public void Create(UnitDto unit)
        {
            var entity = _mapper.Map<Unit>(unit);
            _repository.Create(entity);
        }

        public void Update(UnitDto unit)
        {
            var entity = _mapper.Map<Unit>(unit);
            _repository.Update(entity);
        }

        public void Delete(Guid userId, Guid id)
        {
            _repository.Delete(userId, id);
        }
    }
}
