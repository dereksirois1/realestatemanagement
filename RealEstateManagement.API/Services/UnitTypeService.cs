﻿using AutoMapper;
using RealEstateManagement.API.Models.Dtos;
using RealEstateManagement.API.Repositories.Interfaces;
using RealEstateManagement.API.Services.Interfaces;

namespace RealEstateManagement.API.Services
{
    public class UnitTypeService : IUnitTypeService
    {
        private readonly IUnitTypeRepository _repository;
        private readonly IMapper _mapper;

        public UnitTypeService(IUnitTypeRepository repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        public IEnumerable<UnitTypeDto> GetAll()
        {
            var entities = _repository.GetAll();
            var entitiesDto = _mapper.Map<IEnumerable<UnitTypeDto>>(entities);
            return entitiesDto;
        }

        public UnitTypeDto GetById(Guid id)
        {
            var entity = _repository.GetById(id);
            var entityDto = _mapper.Map<UnitTypeDto>(entity);
            return entityDto;
        }
    }
}
