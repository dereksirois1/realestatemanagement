﻿using AutoMapper;
using RealEstateManagement.API.Models.Dtos;
using RealEstateManagement.API.Models.Entities;
using RealEstateManagement.API.Repositories.Interfaces;
using RealEstateManagement.API.Services.Interfaces;

namespace RealEstateManagement.API.Services
{
    public class MaintenanceService : IMaintenanceService
    {
        private readonly IMaintenanceRepository _repository;
        private readonly IMapper _mapper;

        public MaintenanceService(IMaintenanceRepository repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        public IEnumerable<MaintenanceDto> GetAllByUnit(Guid userId, Guid unitId)
        {
            var maintenances = _repository.GetAllByUnit(userId, unitId);
            var maintenancesDto = _mapper.Map<IEnumerable<MaintenanceDto>>(maintenances);
            return maintenancesDto;
        }

        public MaintenanceDto GetById(Guid userId, Guid id)
        {
            var maintenance = _repository.GetById(userId, id);
            var maintenanceDto = _mapper.Map<MaintenanceDto>(maintenance);
            return maintenanceDto;
        }

        public void Create(MaintenanceDto maintenance)
        {
            var entity = _mapper.Map<Maintenance>(maintenance);
            _repository.Create(entity);
        }

        public void Update(MaintenanceDto maintenance)
        {
            var entity = _mapper.Map<Maintenance>(maintenance);
            _repository.Update(entity);
        }

        public void Delete(Guid userId, Guid id)
        {
            _repository.Delete(userId, id);
        }
    }
}
