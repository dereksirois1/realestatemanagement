﻿using AutoMapper;
using RealEstateManagement.API.Models.Dtos;
using RealEstateManagement.API.Models.Entities;
using RealEstateManagement.API.Repositories.Interfaces;
using RealEstateManagement.API.Services.Interfaces;

namespace RealEstateManagement.API.Services
{
    public class PropertyService : IPropertyService
    {
        private readonly IPropertyRepository _repository;
        private readonly IMapper _mapper;

        public PropertyService(IPropertyRepository repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        public IEnumerable<PropertyDto> GetAllByUser(Guid userId)
        {
            var properties = _repository.GetAllByUser(userId);
            var propertiesDto = _mapper.Map<IEnumerable<PropertyDto>>(properties);
            return propertiesDto;
        }

        public PropertyDto GetById(Guid id)
        {
            var property = _repository.GetById(id);
            var propertyDto = _mapper.Map<PropertyDto>(property);
            return propertyDto;
        }

        public void Create(PropertyDto property)
        {
            var entity = _mapper.Map<Property>(property);
            _repository.Create(entity);
        }

        public void Update(PropertyDto property)
        {
            var entity = _mapper.Map<Property>(property);
            _repository.Update(entity);
        }

        public void Delete(Guid id)
        {
            _repository.Delete(id);
        }
    }
}
