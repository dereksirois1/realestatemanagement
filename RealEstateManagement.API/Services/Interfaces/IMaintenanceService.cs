﻿using RealEstateManagement.API.Models.Dtos;

namespace RealEstateManagement.API.Services.Interfaces
{
    public interface IMaintenanceService
    {
        void Create(MaintenanceDto maintenance);
        void Delete(Guid userId, Guid id);
        IEnumerable<MaintenanceDto> GetAllByUnit(Guid userId, Guid unitId);
        MaintenanceDto GetById(Guid userId, Guid id);
        void Update(MaintenanceDto maintenance);
    }
}