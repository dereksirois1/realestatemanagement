﻿using RealEstateManagement.API.Models.Dtos;

namespace RealEstateManagement.API.Services.Interfaces
{
    public interface IUnitTypeService
    {
        IEnumerable<UnitTypeDto> GetAll();
        UnitTypeDto GetById(Guid id);
    }
}