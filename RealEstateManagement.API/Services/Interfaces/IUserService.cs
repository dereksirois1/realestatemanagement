﻿using RealEstateManagement.API.Models.Dtos;

namespace RealEstateManagement.API.Services.Interfaces
{
    public interface IUserService
    {
        void Create(UserDto user);
        UserDto GetByEmail(string email);
        UserDto GetById(Guid id);
        string Login(UserLoginDto user);
    }
}