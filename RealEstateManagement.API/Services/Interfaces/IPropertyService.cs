﻿using RealEstateManagement.API.Models.Dtos;

namespace RealEstateManagement.API.Services.Interfaces
{
    public interface IPropertyService
    {
        void Create(PropertyDto property);
        void Delete(Guid id);
        IEnumerable<PropertyDto> GetAllByUser(Guid userId);
        PropertyDto GetById(Guid id);
        void Update(PropertyDto property);
    }
}