﻿using RealEstateManagement.API.Models.Dtos;

namespace RealEstateManagement.API.Services.Interfaces
{
    public interface IMaintenanceStatusService
    {
        IEnumerable<MaintenanceStatusDto> GetAll();
        MaintenanceStatusDto GetById(Guid id);
    }
}