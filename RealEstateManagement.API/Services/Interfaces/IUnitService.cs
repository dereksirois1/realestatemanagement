﻿using RealEstateManagement.API.Models.Dtos;

namespace RealEstateManagement.API.Services.Interfaces
{
    public interface IUnitService
    {
        void Create(UnitDto unit);
        void Delete(Guid userId, Guid id);
        IEnumerable<UnitDto> GetAllByProperty(Guid userId, Guid propertyId);
        UnitDto GetById(Guid userId, Guid id);
        void Update(UnitDto unit);
    }
}