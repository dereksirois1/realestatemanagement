﻿namespace RealEstateManagement.API.Config
{
    public record class JwtOptions(
        string Issuer,
        string Audience,
        string SigningKey,
        int ExpirationSeconds);
}
