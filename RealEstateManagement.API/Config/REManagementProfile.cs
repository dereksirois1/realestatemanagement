﻿using AutoMapper;
using RealEstateManagement.API.Models.Dtos;
using RealEstateManagement.API.Models.Entities;

namespace RealEstateManagement.API.Config
{
    public class REManagementProfile : Profile
    {
        public REManagementProfile()
        {
            CreateMap<User, UserDto>().ReverseMap();
            CreateMap<Property, PropertyDto>().ReverseMap();
            CreateMap<UnitType, UnitTypeDto>().ReverseMap();
            CreateMap<Unit, UnitDto>().ReverseMap();
            CreateMap<MaintenanceStatus, MaintenanceStatusDto>().ReverseMap();
            CreateMap<Maintenance, MaintenanceDto>().ReverseMap();
        }
    }
}
