﻿using RealEstateManagement.API.Models.Entities;
using RealEstateManagement.API.Models;
using RealEstateManagement.API.Repositories.Interfaces;

namespace RealEstateManagement.API.Repositories
{
    public class MaintenanceRepository : IMaintenanceRepository
    {
        private readonly REManagementContext _context;

        public MaintenanceRepository(REManagementContext context)
        {
            _context = context;
        }

        public IEnumerable<Maintenance> GetAllByUnit(Guid userId, Guid unitId)
        {
            var maintenances = _context.Maintenance.Where(x => x.Unit.Id == unitId && x.Unit.Property.UserId == userId).ToList();
            return maintenances;
        }

        public Maintenance GetById(Guid userId, Guid id)
        {
            var maintenance = _context.Maintenance.FirstOrDefault(x => x.Id == id && x.Unit.Property.UserId == userId);
            return maintenance;
        }

        public void Create(Maintenance unit)
        {
            _context.Maintenance.Add(unit);
            _context.SaveChanges();
        }

        public void Update(Maintenance unit)
        {
            _context.Maintenance.Update(unit);
            _context.SaveChanges();
        }

        public void Delete(Guid userId, Guid id)
        {
            var entity = GetById(userId, id);
            _context.Remove(entity);
            _context.SaveChanges();
        }
    }
}
