﻿using RealEstateManagement.API.Models.Entities;
using RealEstateManagement.API.Models;
using RealEstateManagement.API.Repositories.Interfaces;

namespace RealEstateManagement.API.Repositories
{
    public class MaintenanceStatusRepository : IMaintenanceStatusRepository
    {
        private readonly REManagementContext _context;
        public MaintenanceStatusRepository(REManagementContext context)
        {
            _context = context;
        }

        public IEnumerable<MaintenanceStatus> GetAll()
        {
            var entities = _context.MaintenanceStatus.ToList();
            return entities;
        }

        public MaintenanceStatus GetById(Guid id)
        {
            var entity = _context.MaintenanceStatus.Find(id);
            return entity;
        }
    }
}
