﻿using RealEstateManagement.API.Models;
using RealEstateManagement.API.Models.Entities;
using RealEstateManagement.API.Repositories.Interfaces;

namespace RealEstateManagement.API.Repositories
{
    public class UserRepository : IUserRepository
    {
        private readonly REManagementContext _context;
        public UserRepository(REManagementContext context)
        {
            _context = context;
        }

        public User? GetById(Guid id)
        {
            var user = _context.Users.Find(id);
            return user;
        }

        public User? GetByEmail(string email)
        {
            var user = _context.Users.Where(u => u.Email == email).FirstOrDefault();
            return user;
        }

        public void Create(User user)
        {
            _context.Users.Add(user);
            _context.SaveChanges();
        }
    }
}
