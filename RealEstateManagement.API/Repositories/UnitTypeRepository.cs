﻿using RealEstateManagement.API.Models;
using RealEstateManagement.API.Models.Entities;
using RealEstateManagement.API.Repositories.Interfaces;

namespace RealEstateManagement.API.Repositories
{
    public class UnitTypeRepository : IUnitTypeRepository
    {
        private readonly REManagementContext _context;
        public UnitTypeRepository(REManagementContext context)
        {
            _context = context;
        }

        public IEnumerable<UnitType> GetAll()
        {
            var entities = _context.UnitType.ToList();
            return entities;
        }

        public UnitType GetById(Guid id)
        {
            var entity = _context.UnitType.Find(id);
            return entity;
        }
    }
}
