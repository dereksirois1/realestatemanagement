﻿using RealEstateManagement.API.Models;
using RealEstateManagement.API.Models.Entities;
using RealEstateManagement.API.Repositories.Interfaces;

namespace RealEstateManagement.API.Repositories
{
    public class PropertyRepository : IPropertyRepository
    {
        private readonly REManagementContext _context;

        public PropertyRepository(REManagementContext context)
        {
            _context = context;
        }

        public IEnumerable<Property> GetAllByUser(Guid userId)
        {
            var properties = _context.Properties.Where(x => x.UserId == userId).ToList();
            return properties;
        }

        public Property GetById(Guid id)
        {
            var property = _context.Properties.FirstOrDefault(x => x.Id == id);
            return property;
        }

        public void Create(Property property)
        {
            _context.Properties.Add(property);
            _context.SaveChanges();
        }

        public void Update(Property property)
        {
            _context.Properties.Update(property);
            _context.SaveChanges();
        }

        public void Delete(Guid id)
        {
            var entity = GetById(id);
            _context.Remove(entity);
            _context.SaveChanges();
        }
    }
}
