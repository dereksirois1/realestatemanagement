﻿using RealEstateManagement.API.Models.Entities;

namespace RealEstateManagement.API.Repositories.Interfaces
{
    public interface IUnitTypeRepository
    {
        IEnumerable<UnitType> GetAll();
        UnitType GetById(Guid id);
    }
}