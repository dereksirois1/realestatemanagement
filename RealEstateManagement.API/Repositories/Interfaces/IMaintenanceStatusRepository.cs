﻿using RealEstateManagement.API.Models.Entities;

namespace RealEstateManagement.API.Repositories.Interfaces
{
    public interface IMaintenanceStatusRepository
    {
        IEnumerable<MaintenanceStatus> GetAll();
        MaintenanceStatus GetById(Guid id);
    }
}