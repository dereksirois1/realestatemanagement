﻿using RealEstateManagement.API.Models.Entities;

namespace RealEstateManagement.API.Repositories.Interfaces
{
    public interface IMaintenanceRepository
    {
        void Create(Maintenance unit);
        void Delete(Guid userId, Guid id);
        IEnumerable<Maintenance> GetAllByUnit(Guid userId, Guid unitId);
        Maintenance GetById(Guid userId, Guid id);
        void Update(Maintenance unit);
    }
}