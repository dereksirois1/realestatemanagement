﻿using RealEstateManagement.API.Models.Entities;

namespace RealEstateManagement.API.Repositories.Interfaces
{
    public interface IUserRepository
    {
        void Create(User user);
        User? GetById(Guid id);
        User? GetByEmail(string email);
    }
}