﻿using RealEstateManagement.API.Models.Entities;

namespace RealEstateManagement.API.Repositories.Interfaces
{
    public interface IUnitRepository
    {
        void Create(Unit unit);
        void Delete(Guid userId, Guid id);
        IEnumerable<Unit> GetAllByProperty(Guid userId, Guid propertyId);
        Unit GetById(Guid userId, Guid id);
        void Update(Unit unit);
    }
}