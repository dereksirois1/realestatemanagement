﻿using RealEstateManagement.API.Models.Entities;

namespace RealEstateManagement.API.Repositories.Interfaces
{
    public interface IPropertyRepository
    {
        void Create(Property property);
        void Delete(Guid id);
        IEnumerable<Property> GetAllByUser(Guid userId);
        Property GetById(Guid id);
        void Update(Property property);
    }
}