﻿using RealEstateManagement.API.Models;
using RealEstateManagement.API.Models.Entities;
using RealEstateManagement.API.Repositories.Interfaces;

namespace RealEstateManagement.API.Repositories
{
    public class UnitRepository : IUnitRepository
    {
        private readonly REManagementContext _context;

        public UnitRepository(REManagementContext context)
        {
            _context = context;
        }

        public IEnumerable<Unit> GetAllByProperty(Guid userId, Guid propertyId)
        {
            var units = _context.Units.Where(x => x.Property.UserId == userId && x.PropertyId == propertyId).ToList();
            return units;
        }

        public Unit GetById(Guid userId, Guid id)
        {
            var units = _context.Units.FirstOrDefault(x => x.Id == id && x.Property.UserId == userId);
            return units;
        }

        public void Create(Unit unit)
        {
            _context.Units.Add(unit);
            _context.SaveChanges();
        }

        public void Update(Unit unit)
        {
            _context.Units.Update(unit);
            _context.SaveChanges();
        }

        public void Delete(Guid userId, Guid id)
        {
            var entity = GetById(userId, id);
            _context.Remove(entity);
            _context.SaveChanges();
        }
    }
}
