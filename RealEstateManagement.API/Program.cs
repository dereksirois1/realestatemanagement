using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using RealEstateManagement.API.Config;
using RealEstateManagement.API.Helper;
using RealEstateManagement.API.Models;
using RealEstateManagement.API.Repositories;
using RealEstateManagement.API.Repositories.Interfaces;
using RealEstateManagement.API.Services;
using RealEstateManagement.API.Services.Interfaces;
using System.Text;

namespace RealEstateManagement.API
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var builder = WebApplication.CreateBuilder(args);

            var jwtOptions = builder.Configuration.GetSection("JwtOptions").Get<JwtOptions>();

            // Add services to the container.

            builder.Services.AddControllers();

            builder.Services.AddAutoMapper(typeof(Program).Assembly);

            builder.Services.AddDbContext<REManagementContext>(o => o.UseSqlServer(builder.Configuration.GetConnectionString("REManagement")));

            builder.Services.AddTransient<IUserRepository, UserRepository>();
            builder.Services.AddTransient<IUserService, UserService>();
            builder.Services.AddTransient<IPropertyRepository, PropertyRepository>();
            builder.Services.AddTransient<IPropertyService, PropertyService>();
            builder.Services.AddTransient<IUnitTypeRepository, UnitTypeRepository>();
            builder.Services.AddTransient<IUnitTypeService, UnitTypeService>();
            builder.Services.AddTransient<IAuthHelper, AuthHelper>();
            builder.Services.AddTransient<IMaintenanceStatusRepository, MaintenanceStatusRepository>();
            builder.Services.AddTransient<IMaintenanceStatusService, MaintenanceStatusService>();
            builder.Services.AddTransient<IMaintenanceService, MaintenanceService>();
            builder.Services.AddTransient<IMaintenanceRepository, MaintenanceRepository>();

            builder.Services.AddSingleton(jwtOptions);

            builder.Services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
            .AddJwtBearer(opts =>
            {
                byte[] signingKeyBytes = Encoding.UTF8
                    .GetBytes(jwtOptions.SigningKey);

                opts.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuer = true,
                    ValidateAudience = true,
                    ValidateLifetime = true,
                    ValidateIssuerSigningKey = true,
                    ValidIssuer = jwtOptions.Issuer,
                    ValidAudience = jwtOptions.Audience,
                    IssuerSigningKey = new SymmetricSecurityKey(signingKeyBytes)
                };
            });

            builder.Services.AddAuthorization();

            var app = builder.Build();

            // Configure the HTTP request pipeline.

            app.UseHttpsRedirection();

            app.UseAuthentication();

            app.UseAuthorization();


            app.MapControllers();

            app.Run();
        }
    }
}
