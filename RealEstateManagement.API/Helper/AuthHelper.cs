﻿using Azure.Core;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using Microsoft.Net.Http.Headers;
using RealEstateManagement.API.Config;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace RealEstateManagement.API.Helper
{
    public class AuthHelper : IAuthHelper
    {
        public string CreateAccessToken(JwtOptions jwtOptions, string email, TimeSpan expiration, Guid userId)
        {
            var keyBytes = Encoding.UTF8.GetBytes(jwtOptions.SigningKey);
            var symmetricKey = new SymmetricSecurityKey(keyBytes);

            var signingCredentials = new SigningCredentials(
                symmetricKey,
                // 👇 one of the most popular. 
                SecurityAlgorithms.HmacSha256);

            var claims = new List<Claim>()
            {
                new Claim("sub", email),
                new Claim("name", email),
                new Claim("aud", jwtOptions.Audience),
                new Claim("id", userId.ToString())
            };

            var token = new JwtSecurityToken(
                issuer: jwtOptions.Issuer,
                audience: jwtOptions.Audience,
                claims: claims,
                expires: DateTime.Now.Add(expiration),
                signingCredentials: signingCredentials);

            var rawToken = new JwtSecurityTokenHandler().WriteToken(token);
            return rawToken;
        }

        public Guid GetAuthenticatedUserId(string token)
        {
            var handler = new JwtSecurityTokenHandler();
            var jwtSecurityToken = handler.ReadJwtToken(token);
            var id = jwtSecurityToken.Claims.First(c => c.Type == "id");
            return Guid.Parse(id.Value);
        }

        public string GetAccessToken(HttpRequest request)
        {
            var accessToken = request.Headers[HeaderNames.Authorization].ToString();
            return accessToken.Substring(7);
        }
    }
}
