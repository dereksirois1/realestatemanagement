﻿using RealEstateManagement.API.Config;

namespace RealEstateManagement.API.Helper
{
    public interface IAuthHelper
    {
        string CreateAccessToken(JwtOptions jwtOptions, string email, TimeSpan expiration, Guid userId);
        Guid GetAuthenticatedUserId(string token);
        string GetAccessToken(HttpRequest request);
    }
}