﻿using RealEstateManagement.API.Config;
using RealEstateManagement.API.Helper;

namespace RealEstateManagement.Test
{
    public class AuthHelperTests
    {
        private readonly IAuthHelper _authHelper;
        private readonly JwtOptions _jwtOptions;
        public AuthHelperTests()
        {
            _authHelper = new AuthHelper();
            _jwtOptions = new JwtOptions("https://localhost:7244", "https://localhost:7244", "6F45A6D4FFE7A4A1A23EB3FAD8FEBEBEBEB", 3600);
        }

        [Fact]
        public void ShouldCreateToken()
        {
            var token = _authHelper.CreateAccessToken(_jwtOptions, "bob@gmail.com", TimeSpan.FromMicroseconds(1000), Guid.NewGuid());
            token.Should().NotBeNull();
            token.Should().NotBeEmpty();
            token.StartsWith("ey").Should().BeTrue();
        }

        [Fact]
        public void ShouldGetUserIdFromToken()
        {
            var userId = Guid.NewGuid();
            var token = _authHelper.CreateAccessToken(_jwtOptions, "bob@gmail.com", TimeSpan.FromMicroseconds(1000), userId);
            var id = _authHelper.GetAuthenticatedUserId(token);
            id.Should().Be(userId);
        }
    }
}
